FROM python:3.7-alpine

ARG environment

ENV PYTHONUNBUFFERED 1
ENV FLASK_ENV ${environment}

# Setup workdir
RUN mkdir /app
WORKDIR /app

# Install system deps
RUN apk add --update --no-cache postgresql-dev gcc python3-dev musl-dev

# Install python libs
COPY requirements/ /app/requirements/
RUN pip install --no-cache-dir -r requirements/${environment}.txt

COPY . /app

# run app
EXPOSE 5000
CMD ["gunicorn", "-b", "0.0.0.0:5000", "wishlist_api.wsgi"]
