ifndef ENV
	override ENV = development
endif

.PHONY: createdb dbconsole runapi gunicorn apiconsole shell test testcoverage lint rebuild

createdb:
	docker-compose up -d db
	docker-compose exec db createdb -U postgres "wishlist_api_${ENV}"

dbconsole:
	docker-compose up -d db
	docker-compose exec db psql -U postgres "wishlist_api_${ENV}"

runapi:
	docker-compose run --service-ports api flask run --host=0.0.0.0

gunicorn:
	docker-compose up api

apiconsole:
	docker-compose run api flask shell

shell:
	docker-compose run api sh

test:
	docker-compose run api pytest --cov wishlist_api tests

testcoverage:
	docker-compose run api pytest --cov-report html --cov wishlist_api tests
	firefox htmlcov/index.html

lint:
	docker-compose run api pylint wishlist_api/

rebuild:
	docker-compose build api
