from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='wishlist_api',
    version='0.1.0',
    description='LuizaLabs recruiting challenge -- Customer product wishlist API',
    long_description=readme,
    author='Marcos Costa Pinto',
    author_email='costa.marcos.pro@gmail.com',
    url='https://bitbucket.org/marcospy/luizalabs-wishlist-api',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
