import logging
import os
import sys


logger = logging.getLogger('wishlist_api')
logger.setLevel(os.getenv('LOG_LEVEL', 'INFO'))
logger.addHandler(logging.StreamHandler(sys.stdout))

