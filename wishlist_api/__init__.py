import json_logging
from flask import Flask, jsonify
from flask_migrate import Migrate
from wishlist_api.auth import init_jwt
from wishlist_api.models import db
from wishlist_api.views import bp as api_blueprint
from wishlist_api.logging import logger


def create_app(test_config=None):
    app = Flask(__name__)

    if test_config is None:
        app.config.from_object('wishlist_api.config')

        # Setup json logging
        json_logging.ENABLE_JSON_LOGGING = True
        json_logging.init_flask()
        json_logging.init_request_instrument(app)
    else:
        app.config.from_mapping(test_config)

    # Setup database
    db.init_app(app)

    # Setup migrations
    migrate = Migrate(app, db)

    # Register api endpoints
    app.register_blueprint(api_blueprint, url_prefix='/api/v1')

    # Setup JWT
    init_jwt(app)

    # Health check
    @app.route('/health-check')
    def health():
        logger.info('app is healthy')
        return jsonify({'status': 'healthy'})

    return app
