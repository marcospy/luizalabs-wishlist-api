import os

SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI')
SQLALCHEMY_TRACK_MODIFICATIONS = False
JWT_SECRET_KEY=os.getenv('JWT_SECRET_KEY') # use assymetric signature if using third party OAuth server
