from marshmallow import Schema, fields, post_load, validate, validates, ValidationError
from wishlist_api.models import Product


class CustomerSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True, validate=validate.Length(min=1))
    email = fields.Str(required=True, validate=validate.Email())


class ProductSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(required=True, validate=validate.Length(min=1))
    brand = fields.Str(required=True, validate=validate.Length(min=1))
    price_cents = fields.Int(required=True, validate=validate.Range(min=1))
    image = fields.Str(required=True, validate=validate.URL())
    review_score = fields.Int(required=True, validate=validate.Range(min=1, max=5))


class ProductUpdateSchema(Schema):
    title = fields.Str(validate=validate.Length(min=1))
    brand = fields.Str(validate=validate.Length(min=1))
    price_cents = fields.Int(validate=validate.Range(min=1))
    image = fields.Str(validate=validate.URL())
    review_score = fields.Int(validate=validate.Range(min=1, max=5))


class WishlistItemSchema(Schema):
    id = fields.Int(dump_only=True)
    product_id = fields.Int()

    @validates('product_id')
    def validate_proudct_id(self, value):
        if Product.query.get(value) is None:
            raise ValidationError('Product does not exist.')


class WishlistSchema(Schema):
    id = fields.Int()
    customer_id = fields.Int()
    items = fields.Nested(WishlistItemSchema, many=True)


customer_schema = CustomerSchema()
customer_update_schema = CustomerSchema(exclude=['email'])
customers_schema = CustomerSchema(many=True)
product_schema = ProductSchema()
product_update_schema = ProductUpdateSchema()
products_schema = ProductSchema(many=True)
wishlist_schema = WishlistSchema()
wishlist_item_schema = WishlistItemSchema()
