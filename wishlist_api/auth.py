from functools import wraps
from flask_jwt_extended import JWTManager, get_jwt_claims, get_jwt_identity
from wishlist_api.models import User


SCOPES = {
    'customer': [
        'customer:show',
        'customer:update',
        'product:list',
        'product:read',
        'wishlist:read',
        'wishlist_item:create',
        'wishlist_item:delete'
    ],
    'admin': [
        'customer:list',
        'customer:show',
        'customer:create',
        'customer:update',
        'customer:delete',
        'product:list',
        'product:show',
        'product:create',
        'product:update',
        'product:delete',
        'wishlist:read',
        'wishlist_item:create',
        'wishlist_item:delete'
    ]
}


def init_jwt(app):
    jwt = JWTManager(app)

    @jwt.user_claims_loader
    def assign_user_claims(identity):
        return {
            'scopes': SCOPES[identity['type']]
        }


def get_current_user():
    identity = get_jwt_identity()
    return User(identity['id'], identity['type'])
