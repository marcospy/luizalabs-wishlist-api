from flask import request, Blueprint, jsonify, abort
from marshmallow import ValidationError as SchemaValidationError
from flask_jwt_extended import jwt_required, get_jwt_identity, get_jwt_claims
from wishlist_api.logging import logger
from wishlist_api.models import db, Customer, Product, Wishlist, WishlistItem
from wishlist_api.auth import get_current_user
from wishlist_api.view_helpers import paginated, ApiError, require_scopes, require_admin_or_customer
from wishlist_api.schemas import (
    customer_schema, customer_update_schema, customers_schema,
    product_schema, product_update_schema, products_schema,
    wishlist_schema, wishlist_item_schema
)


bp = Blueprint('api', __name__)


# Customer routes
@bp.route('/customers')
@jwt_required
@paginated
def list_customers(page, per_page):
    require_scopes('customer:list')
    results = Customer.query.paginate(page, per_page).items
    return jsonify(data=customers_schema.dump(results))


@bp.route('/customers/<int:customer_id>')
@jwt_required
def show_customer(customer_id):
    require_scopes('customer:show')
    require_admin_or_customer(customer_id)
    customer = Customer.query.get_or_404(customer_id)
    return jsonify(data=customer_schema.dump(customer))


@bp.route('/customers', methods=['POST'])
@jwt_required
def create_customer():
    require_scopes('customer:create')
    current_user = get_current_user()
    customer = Customer(**customer_schema.load(request.json))

    if Customer.already_exists(customer.email):
        raise ApiError({'email': ['Email already in use.']}, 400)

    db.session.add(customer)
    db.session.commit()
    logger.info('{} created by {}'.format(customer, current_user))
    return jsonify(data=customer_schema.dump(customer)), 201


@bp.route('/customers/<int:customer_id>', methods=['PATCH'])
@jwt_required
def update_customer(customer_id):
    require_scopes('customer:update')
    require_admin_or_customer(customer_id)
    current_user = get_current_user()
    new_data = customer_update_schema.load(request.json)
    customer = Customer.query.get_or_404(customer_id)
    customer.name = new_data['name']
    db.session.commit()
    logger.info('{} updated by {}'.format(customer, current_user))
    return jsonify(data=customer_schema.dump(customer))


@bp.route('/customers/<int:customer_id>', methods=['DELETE'])
@jwt_required
def delete_customer(customer_id):
    require_scopes('customer:delete')
    current_user = get_current_user()
    customer = Customer.query.get_or_404(customer_id)
    db.session.delete(customer)
    db.session.commit()
    logger.info('{} updated by {}'.format(customer, current_user))
    return jsonify(data=customer_schema.dump(customer))


# Product routes
@bp.route('/products')
@jwt_required
@paginated
def list_products(page, per_page):
    require_scopes('product:list')
    products = Product.query.paginate(page, per_page).items
    results = products_schema.dump(products)
    return jsonify(data=results)


@bp.route('/products/<int:product_id>')
@jwt_required
def show_product(product_id):
    require_scopes('product:show')
    product = Product.query.get_or_404(product_id)
    return jsonify(data=product_schema.dump(product))


@bp.route('/products', methods=['POST'])
@jwt_required
def create_product():
    require_scopes('product:create')
    current_user = get_current_user()
    product = Product(**product_schema.load(request.json))
    db.session.add(product)
    db.session.commit()
    logger.info('{} created by {}'.format(product, current_user))
    return jsonify(data=product_schema.dump(product)), 201


@bp.route('/products/<int:product_id>', methods=['PUT', 'PATCH'])
@jwt_required
def update_product(product_id):
    require_scopes('product:update')
    current_user = get_current_user()
    product = Product.query.get_or_404(product_id)

    for key, value in product_update_schema.load(request.json).items():
        setattr(product, key, value)

    db.session.commit()
    logger.info('{} updated by {}'.format(product, current_user))
    return jsonify(data=product_schema.dump(product))


@bp.route('/products/<int:product_id>', methods=['DELETE'])
@jwt_required
def delete_product(product_id):
    require_scopes('product:delete')
    current_user = get_current_user()
    product = Product.query.get_or_404(product_id)
    db.session.delete(product)
    db.session.commit()
    logger.info('{} deleted by {}'.format(product, current_user))
    return jsonify(data=product_schema.dump(product))


# Wishlist routes
@bp.route('/customers/<int:customer_id>/wishlist')
@jwt_required
def get_customer_wishlist(customer_id):
    require_scopes('wishlist:read')
    require_admin_or_customer(customer_id)
    customer = Customer.query.get_or_404(customer_id)
    return jsonify(data=wishlist_schema.dump(customer.wishlist))


@bp.route('/customers/<int:customer_id>/wishlist/items', methods=['POST'])
@jwt_required
def add_wishlist_item(customer_id):
    require_scopes('wishlist_item:create')
    require_admin_or_customer(customer_id)
    current_user = get_current_user()

    wishlist = Customer.query.get_or_404(customer_id).wishlist
    item = WishlistItem(**wishlist_item_schema.load(request.json))

    if wishlist.contains_item(item):
        raise ApiError({'product_id': ['Product already on the wishlist.']}, 400)

    wishlist.items.append(item)
    db.session.commit()
    logger.info('{} created by {}'.format(item, current_user))
    return jsonify(data=wishlist_item_schema.dump(item)), 201


# TODO: adicionar rota pra remover item da wishlist
@bp.route('/customers/<int:customer_id>/wishlist/items/<int:item_id>', methods=['DELETE'])
@jwt_required
def delete_wishlist_item(customer_id, item_id):
    require_scopes('wishlist_item:delete')
    require_admin_or_customer(customer_id)
    item = db.session.query(WishlistItem).join(Wishlist).filter(Wishlist.customer_id == customer_id).filter(WishlistItem.id == item_id).first()

    if item is None:
        abort(404)

    db.session.delete(item)
    db.session.commit()
    logger.info('{} deleted by {}'.format(item, get_current_user()))
    return jsonify(data=wishlist_item_schema.dump(item))


# Error handlers
@bp.errorhandler(404)
def handle_resource_not_found(e):
    return jsonify({'errors': {'not_found': ['Requested resource not found.']}}), 404


@bp.errorhandler(SchemaValidationError)
def handle_schema_validation_error(e):
    logger.info('validation error ocurred {}'.format(e.messages))
    return jsonify({'errors': e.messages}), 400


@bp.errorhandler(ApiError)
def handle_api_error(e):
    logger.info('api error ocurred {}'.format(e.errors))
    return jsonify({'errors': e.errors}), e.status_code
