from flask import request, abort
from functools import wraps
from flask_jwt_extended import get_jwt_claims
from wishlist_api.auth import get_current_user


class ApiError(Exception):
    def __init__(self, errors, status_code):
        self.errors = errors
        self.status_code = status_code


def safe_cast(value, to_type, default = None):
    """Cast types safely."""
    try:
        return to_type(value)
    except (ValueError, TypeError):
        return default


def paginated(fn, default_per_page = 100, max_per_page = 1000):
    """
    Validate and insert pagination params on view.

    Usage example:
        @paginated
        def my_view(page, per_page):
            pass

        @paginated(default_per_page = 10, max_per_page=100)
        def my_view(page, per_page):
            pass
    """
    @wraps(fn)
    def decorated(*args, **kwargs):
        kwargs['page'] = safe_cast(request.args.get('page'), int, 1)
        kwargs['per_page'] = min(safe_cast(request.args.get('per_page'), int, default_per_page), max_per_page)
        return fn(*args, **kwargs)

    return decorated


def require_scopes(*scopes, **kwargs):
    """Check if the jwt has the required scopes. By default it will raise an ApiError when any scope is missing."""
    claims = get_jwt_claims()
    token_scopes = claims.get('scopes', [])

    if len(set(token_scopes) & set(scopes)) != len(scopes):
        if kwargs.get('raise_error', True):
            raise ApiError({'authorization': ['Access denied to the requested resource.']}, 403)
        return False
    return True


def require_admin_or_customer(customer_id):
    """Raises 404 if current user is not an admin nor the given customer"""
    user = get_current_user()
    if not (user.is_admin() or (user.is_customer() and user.id == customer_id)):
        abort(404)
