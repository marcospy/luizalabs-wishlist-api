from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Customer(db.Model):
    __tablename__ = 'customers'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.Unicode(256), nullable=False)
    email = db.Column(db.String(256), nullable=False, unique=True, index=True)

    @property
    def wishlist(self):
        wishlist = Wishlist.query.filter(Wishlist.customer_id == self.id).first()
        if wishlist is not None:
            return wishlist

        new_wishlist = Wishlist(customer_id=self.id)
        db.session.add(new_wishlist)
        db.session.commit()
        return new_wishlist


    @staticmethod
    def already_exists(email):
        return Customer.query.filter(Customer.email == email).first() is not None


class Product(db.Model):
    __tablename__ = 'products'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.Unicode(), nullable=False)
    brand = db.Column(db.Unicode(), nullable=False)
    price_cents = db.Column(db.Integer(), nullable=False)
    image = db.Column(db.String(), nullable=False)
    review_score = db.Column(db.SmallInteger(), nullable=False)


class Wishlist(db.Model):
    __tablename__ = 'wishlists'

    id = db.Column(db.Integer(), primary_key=True)
    customer_id = db.Column(db.Integer(), db.ForeignKey('customers.id'), unique=True, index=True, nullable=False)
    items = db.relationship('WishlistItem', lazy='dynamic')

    def contains_item(self, item):
        return any(i.product_id == item.product_id for i in self.items)


class WishlistItem(db.Model):
    __tablename__ = 'wishlist_items'
    __table_args__ = (
        db.UniqueConstraint('wishlist_id', 'product_id', name='wishlist_items_composity_key'),
    )

    id = db.Column(db.Integer(), primary_key=True)
    wishlist_id = db.Column(db.Integer(), db.ForeignKey('wishlists.id'), nullable=False)
    wishlist = db.relationship('Wishlist')
    product_id = db.Column(db.Integer(), db.ForeignKey('products.id'), nullable=False)
    product = db.relationship('Product')


class User(object):
    def __init__(self, id, type):
        self.id = id
        self.type = type

    def is_admin(self):
        return self.type == 'admin'

    def is_customer(self):
        return self.type == 'customer'

    def __repr__(self):
        return 'User<id:{}, type:{}>'.format(self.id, self.type)
