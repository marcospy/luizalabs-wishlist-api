import factory
from sqlalchemy.orm import sessionmaker
from wishlist_api.models import Customer, Product, Wishlist, WishlistItem


class CustomerFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Customer

    id = factory.Sequence(lambda n: n)
    name = factory.Faker('name')
    email = factory.Faker('email')



class ProductFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Product

    id = factory.Sequence(lambda n: n)
    title = 'IPhone'
    brand = 'Apple'
    price_cents = 10000
    image = factory.Faker('url')
    review_score = 5
