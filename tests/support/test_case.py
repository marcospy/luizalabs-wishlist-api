from flask_testing import TestCase
from wishlist_api import create_app, db
from tests.support.factories import CustomerFactory, ProductFactory


class TestCase(TestCase):
    APP_CONFIG = {
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': 'sqlite://',
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'JWT_SECRET_KEY': 'TEST_SECRET'
    }

    def create_app(self):
        return create_app(self.APP_CONFIG)

    def setUp(self):
        db.create_all()
        CustomerFactory._meta.sqlalchemy_session = db.session
        ProductFactory._meta.sqlalchemy_session = db.session

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def assertOk(self, response, expected_response=None):
        self.assertResponse(response, 200, expected_response)

    def assertCreated(self, response, expected_response=None):
        self.assertResponse(response, 201, expected_response)

    def assertBadRequest(self, response, expected_response=None):
        self.assertResponse(response, 400, expected_response)

    def assertUnauthorized(self, response):
        self.assertResponse(response, 401)

    def assertForbidden(self, response):
        self.assertResponse(response, 403, {'errors': {'authorization': ['Access denied to the requested resource.']}})

    def assertNotFound(self, response):
        self.assertResponse(response, 404, {'errors': {'not_found': ['Requested resource not found.']}})

    def assertResponse(self, response, expected_status_code, expected_body=None):
        self.assertEqual(response.status_code, expected_status_code)

        if expected_body is not None:
            self.assertEqual(response.get_json(), expected_body)

