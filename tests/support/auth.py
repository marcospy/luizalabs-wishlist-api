from flask_jwt_extended import create_access_token


def get_jwt_header(user_id, user_type):
    token = create_access_token(identity={'id': user_id, 'type': user_type})
    return {'Authorization': 'Bearer {}'.format(token)}


def admin_jwt_header(user_id=1):
    return get_jwt_header(user_id, 'admin')


def customer_jwt_header(user_id=1):
    return get_jwt_header(user_id, 'customer')
