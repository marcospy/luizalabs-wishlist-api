import json
from wishlist_api.models import db, Wishlist, WishlistItem
from tests.support.test_case import TestCase
from tests.support.factories import CustomerFactory, ProductFactory
from tests.support.auth import admin_jwt_header, customer_jwt_header


class WishlistViewsTests(TestCase):
    def test_get_customer_wishlist(self):
        # when not authenticated
        self.assertUnauthorized(self.client.get('/api/v1/customers/1/wishlist'))

        # when customer does not exist
        self.assertNotFound(self.client.get('/api/v1/customers/1/wishlist', headers=admin_jwt_header()))

        # when customer exists but wishlist not
        customer = CustomerFactory.create()
        response = self.client.get(
            '/api/v1/customers/{}/wishlist'.format(customer.id), headers=customer_jwt_header(customer.id))
        self.assertOk(response, {
            'data': {
                'id': customer.wishlist.id,
                'customer_id': customer.id,
                'items': []
            }
        })
        self.assertEqual(Wishlist.query.count(), 1) # creates a new wishlist

        # when wishlist already exists
        response = self.client.get(
            '/api/v1/customers/{}/wishlist'.format(customer.id), headers=customer_jwt_header(customer.id))
        self.assertEqual(Wishlist.query.count(), 1) # reuse the wishlist
        self.assertOk(response, {
            'data': {
                'id': customer.wishlist.id,
                'customer_id': customer.id,
                'items': []
            }
        })

        # when wishlist contains some items
        product = ProductFactory.create()
        item = WishlistItem(product_id=product.id)
        customer.wishlist.items.append(item)
        db.session.commit()

        response = self.client.get(
            '/api/v1/customers/{}/wishlist'.format(customer.id), headers=customer_jwt_header(customer.id))
        self.assertOk(response, {
            'data': {
                'id': customer.wishlist.id,
                'customer_id': customer.id,
                'items': [{
                    'id': item.id,
                    'product_id': product.id
                }]
            }
        })

    def test_add_wishlist_item(self):
        # when not authenticated
        self.assertUnauthorized(self.client.post('/api/v1/customers/1/wishlist/items'))

        # when customer does not exist
        self.assertNotFound(self.client.post('/api/v1/customers/1/wishlist/items', headers=customer_jwt_header()))

        # when product does not exist
        customer = CustomerFactory.create()
        product = ProductFactory.create()
        response = self.client.post(
            '/api/v1/customers/{}/wishlist/items'.format(customer.id),
            data=json.dumps({'product_id': 1}),
            content_type='application/json',
            headers=customer_jwt_header(customer.id)
        )
        self.assertBadRequest(response, {
            'errors': {
                'product_id': ['Product does not exist.']
            }
        })

        # when item is not yet present on the customer wishlist
        customer = CustomerFactory.create()
        product = ProductFactory.create()
        response = self.client.post(
            '/api/v1/customers/{}/wishlist/items'.format(customer.id),
            data=json.dumps({'product_id': product.id}),
            content_type='application/json',
            headers=customer_jwt_header(customer.id)
        )
        self.assertEqual(WishlistItem.query.count(), 1) # creates a new wishlist
        self.assertCreated(response, {
            'data': {
                'id': WishlistItem.query.first().id,
                'product_id': product.id
            }
        })

        # when items already on the wishlist
        response = self.client.post(
            '/api/v1/customers/{}/wishlist/items'.format(customer.id),
            data=json.dumps({'product_id': product.id}),
            content_type='application/json',
            headers=customer_jwt_header(customer.id)
        )
        self.assertBadRequest(response, {
            'errors': {
                'product_id': ['Product already on the wishlist.']
            }
        })

    def test_delete_wishlist_item(self):
        # when not authenticated
        self.assertUnauthorized(self.client.delete('/api/v1/customers/1/wishlist/items/1'))

        # when item does not exist
        customer = CustomerFactory.create()
        self.assertNotFound(self.client.delete(
            '/api/v1/customers/{}/wishlist/items/1'.format(customer.id), headers=customer_jwt_header(customer.id)))

        # when item exists
        product = ProductFactory.create()
        item = WishlistItem(product_id=product.id)
        customer.wishlist.items.append(item)
        db.session.commit()

        response = self.client.delete(
            '/api/v1/customers/{}/wishlist/items/1'.format(customer.id), headers=customer_jwt_header(customer.id))
        self.assertOk(response)
