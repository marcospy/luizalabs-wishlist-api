import json
from wishlist_api.models import Customer
from tests.support.test_case import TestCase
from tests.support.factories import CustomerFactory
from tests.support.auth import admin_jwt_header, customer_jwt_header


class CustomerViewsTests(TestCase):
    def test_list_customers(self):
        # when not authenticated
        self.assertUnauthorized(self.client.get('/api/v1/customers'))

        # when authenticated as admin
        customer = CustomerFactory.create()
        response = self.client.get('/api/v1/customers', headers=admin_jwt_header())

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {
            'data': [
                {'id': customer.id, 'name': customer.name, 'email': customer.email}
            ]
        })

        # when authenticated as customer
        response = self.client.get('/api/v1/customers', headers=customer_jwt_header(customer.id))
        self.assertForbidden(response)

    def test_list_customers_pagination(self):
        [CustomerFactory.create() for i in range(3)]

        # default pagination
        response = self.client.get('/api/v1/customers', headers=admin_jwt_header()).get_json()
        self.assertEqual(len(response['data']), 3)

        # with invalid pagination params
        response = self.client.get('/api/v1/customers?page=abcd', headers=admin_jwt_header()).get_json()
        self.assertEqual(len(response['data']), 3)

        # setting per_page
        response = self.client.get('/api/v1/customers?per_page=2', headers=admin_jwt_header()).get_json()
        self.assertEqual(len(response['data']), 2)

        # setting page
        response = self.client.get('/api/v1/customers?per_page=2&page=2', headers=admin_jwt_header()).get_json()
        self.assertEqual(len(response['data']), 1)

    def test_show_customer(self):
        # when not authenticated
        self.assertUnauthorized(self.client.get('/api/v1/customers/1'))

        # when customer does not exist
        response = self.client.get('/api/v1/customers/1', headers=admin_jwt_header())
        self.assertNotFound(response)

        # when customer exists
        customer = CustomerFactory.create()
        response = self.client.get('/api/v1/customers/{}'.format(customer.id), headers=admin_jwt_header())
        self.assertOk(response, {
            'data': {
                'id': customer.id,
                'name': customer.name,
                'email': customer.email
            }
        })

        # when authenticated with the same customer
        response = self.client.get('/api/v1/customers/{}'.format(customer.id), headers=customer_jwt_header(customer.id))
        self.assertOk(response)

        # when authenticated with a different customer
        response = self.client.get('/api/v1/customers/{}'.format(customer.id), headers=customer_jwt_header(2))
        self.assertNotFound(response)

    def test_create_customer(self):
        # when not authenticated
        self.assertUnauthorized(self.client.post('/api/v1/customers'))

        # when input invalid
        response = self.client.post(
            '/api/v1/customers',
            data=json.dumps({'name': '', 'email': ''}),
            content_type='application/json',
            headers=admin_jwt_header())
        self.assertBadRequest(response, {
            'errors': {
                'email': ['Not a valid email address.'],
                'name': ['Shorter than minimum length 1.']
            }
        })

        # when input valid
        valid_input = json.dumps({'name': 'test', 'email': 'test@email.com'})

        # when authenticated as customer
        response = self.client.post(
            '/api/v1/customers',
            data=valid_input,
            content_type='application/json',
            headers=customer_jwt_header())
        self.assertForbidden(response)

        # when authenticated as admin
        response = self.client.post(
            '/api/v1/customers',
            data=valid_input,
            content_type='application/json',
            headers=admin_jwt_header())
        self.assertCreated(response, {
            'data': {
                'id': Customer.query.first().id,
                'name': 'test',
                'email': 'test@email.com'
            }
        })

        # when email duplicated
        response = self.client.post(
            '/api/v1/customers',
            data=json.dumps({'name': 'Test', 'email': 'test@email.com'}),
            content_type='application/json',
            headers=admin_jwt_header())
        self.assertBadRequest(response, {
            'errors': {
                'email': ['Email already in use.']
            }
        })

    def test_update_customer(self):
        # when not authenticated
        response = self.client.patch(
            '/api/v1/customers/1',
            data=json.dumps({'name': 'Test'}),
            content_type='application/json')
        self.assertUnauthorized(response)

        # when customer does not exist
        response = self.client.patch(
            '/api/v1/customers/1',
            data=json.dumps({'name': 'Test'}),
            content_type='application/json',
            headers=admin_jwt_header())
        self.assertNotFound(response)

        # when customer exists
        customer = CustomerFactory.create()

        # when authenticated as different customer
        original_name = customer.name
        response = self.client.patch(
            '/api/v1/customers/{}'.format(customer.id),
            data=json.dumps({'name': 'Test'}),
            content_type='application/json',
            headers=customer_jwt_header(customer.id + 1))
        self.assertNotFound(response)

        # when authenticated as the correct customer
        original_name = customer.name
        response = self.client.patch(
            '/api/v1/customers/{}'.format(customer.id),
            data=json.dumps({'name': 'Test'}),
            content_type='application/json',
            headers=customer_jwt_header(customer.id))
        self.assertOk(response, {
            'data': {
                'id': customer.id,
                'email': customer.email,
                'name': 'Test'
            }
        })
        persisted_customer = Customer.query.first()
        self.assertEqual(persisted_customer.name, 'Test')

        # when unauthorized field passed
        response = self.client.patch(
            '/api/v1/customers/{}'.format(customer.id),
            data=json.dumps({'email': 'test@email.com', 'name': 'Test'}),
            content_type='application/json',
            headers=customer_jwt_header(customer.id))
        self.assertBadRequest(response, {
            'errors': {
                'email': ['Unknown field.']
            }
        })

    def test_delete_customer(self):
        # when not authenticated
        self.assertUnauthorized(self.client.delete('/api/v1/customers/1'))

        # when customer does not exist
        response = self.client.delete('/api/v1/customers/1', headers=admin_jwt_header())
        self.assertNotFound(response)

        # when customer exists
        customer = CustomerFactory.create()

        # when authenticated as customer
        response = self.client.delete(
            '/api/v1/customers/{}'.format(customer.id),
            headers=customer_jwt_header(customer.id))
        self.assertForbidden(response)
        self.assertEqual(Customer.query.count(), 1)

        # when authenticated as admin
        response = self.client.delete('/api/v1/customers/{}'.format(customer.id), headers=admin_jwt_header())
        self.assertOk(response)
        self.assertEqual(Customer.query.count(), 0)
