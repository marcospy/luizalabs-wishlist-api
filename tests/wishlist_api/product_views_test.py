import json
from wishlist_api.models import Product
from tests.support.test_case import TestCase
from tests.support.factories import ProductFactory
from tests.support.auth import admin_jwt_header


class ProductViewsTests(TestCase):
    def test_list_products(self):
        # when not authenticated
        self.assertUnauthorized(self.client.get('/api/v1/products'))

        # when authenticated as admin
        product = ProductFactory.create()
        response = self.client.get('/api/v1/products', headers=admin_jwt_header())
        self.assertOk(response, {
            'data': [
                {
                    'id': product.id, 'title': product.title, 'brand': product.brand,
                    'price_cents': product.price_cents, 'image': product.image, 'review_score': product.review_score
                }
            ]
        })

    def test_list_products_pagination(self):
        [ProductFactory.create() for i in range(3)]

        # default pagination
        response = self.client.get('/api/v1/products', headers=admin_jwt_header()).get_json()
        self.assertEqual(len(response['data']), 3)

        # with invalid pagination params
        response = self.client.get('/api/v1/products?page=abcd', headers=admin_jwt_header()).get_json()
        self.assertEqual(len(response['data']), 3)

        # setting per_page
        response = self.client.get('/api/v1/products?per_page=2', headers=admin_jwt_header()).get_json()
        self.assertEqual(len(response['data']), 2)

        # setting page
        response = self.client.get('/api/v1/products?per_page=2&page=2', headers=admin_jwt_header()).get_json()
        self.assertEqual(len(response['data']), 1)

    def test_show_product(self):
        # when not authenticated
        self.assertUnauthorized(self.client.get('/api/v1/products/1'))

        # when product does not exist
        self.assertNotFound(self.client.get('/api/v1/products/1', headers=admin_jwt_header()))

        # when product exists
        product = ProductFactory.create()
        response = self.client.get('/api/v1/products/{}'.format(product.id), headers=admin_jwt_header())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_json(), {
            'data': {
                'id': product.id, 'title': product.title, 'brand': product.brand,
                'price_cents': product.price_cents, 'image': product.image, 'review_score': product.review_score
            }
        })

    def test_create_product(self):
        # when not authenticated
        self.assertUnauthorized(self.client.post('/api/v1/products'))

        # when input invalid
        response = self.client.post(
            '/api/v1/products', data=json.dumps({}), content_type='application/json', headers=admin_jwt_header())
        self.assertBadRequest(response, {
            'errors': {
                'brand': ['Missing data for required field.'],
                'title': ['Missing data for required field.'],
                'price_cents': ['Missing data for required field.'],
                'image': ['Missing data for required field.'],
                'review_score': ['Missing data for required field.']
            }
        })

        # when input valid
        params = {
            'title': 'IPhone',
            'brand': 'Apple',
            'price_cents': 100000,
            'image': 'http://image.com.br/pic.jpg',
            'review_score': 5
        }
        response = self.client.post(
            '/api/v1/products', data=json.dumps(params), content_type='application/json', headers=admin_jwt_header())
        self.assertCreated(response, {'data': dict(id=Product.query.first().id, **params)})

    def test_update_product(self):
        # when not authenticated
        self.assertUnauthorized(self.client.patch('/api/v1/products/1'))

        # when product does not exist
        response = self.client.patch(
            '/api/v1/products/1',
            data=json.dumps({'title': 'IPad'}),
            content_type='application/json',
            headers=admin_jwt_header())
        self.assertNotFound(response)

        # when product exists
        product = ProductFactory.create()
        response = self.client.patch(
            '/api/v1/products/{}'.format(product.id),
            data=json.dumps({'title': 'IPad'}),
            content_type='application/json',
            headers=admin_jwt_header())
        self.assertOk(response, {
            'data': {
                'id': product.id,
                'title': 'IPad',
                'brand': product.brand,
                'price_cents': product.price_cents,
                'image': product.image,
                'review_score': product.review_score
            }
        })
        self.assertEqual(product.query.first().title, 'IPad')

    def test_delete_product(self):
        # when not authenticated
        self.assertUnauthorized(self.client.delete('/api/v1/products/1'))

        # when product does not exist
        self.assertNotFound(self.client.delete('/api/v1/products/1', headers=admin_jwt_header()))

        # when product exists
        product = ProductFactory.create()
        response = self.client.delete('/api/v1/products/{}'.format(product.id), headers=admin_jwt_header())
        self.assertOk(response)
        self.assertEqual(product.query.count(), 0)
