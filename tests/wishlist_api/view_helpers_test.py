import pytest
from unittest.mock import patch
from wishlist_api.view_helpers import safe_cast, paginated


def test_safe_cast():
    assert safe_cast('1', int) == 1
    assert safe_cast('abc', int, 1) == 1
    assert safe_cast('abc', int) is None
