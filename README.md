# luizalabs-wishlist-api

Desafio do processo seletivo do LuizaLabs


## Considerações sobre o que foi desenvolvido

Vejo que o procedimento mais seguro para autenticação da solução como um todo seja utilizar um servidor OAuth 2.0 com o
esquema [PKCE](https://tools.ietf.org/html/rfc7636) para garantir segurança na utilização de tokens de acesso utilizados
para acessar o provedor de recursos, que no caso é a API da wishlist.

Foquei nos testes automatizados da API, pois no tempo que tinha disponível eram os que garantiriam o correto
funcionamento da API. Num ambiente normal eu costumo testar helpers e outros elementos.

## TODO

- Automatizar deploy;
- Automatizar pipeline de CI/CD;
- Adicionar APM com Datadog ou NewRelic;
- Criar documentação da API com Slate ou Swagger;
- Implementar servidor OAuth2 para autenticação e autorização;
- Otimizar camadas da imagem Docker;
- Tratar erros do JWT corretamente;
- Atender melhor à especificação [JSON:API](https://jsonapi.org/);


## Desenvolvimento

Siga os próximos para executar a aplicação em desenvolvimento.


### Requisitos

Todos os comandos abaixo assumem que você já esteja com o [docker-compose](https://docs.docker.com/compose/)
e com o [make](https://www.gnu.org/software/make/manual/make.html) instalados em sua máquina.


### Crie o banco de dados

Suba o serviço de banco de dados:

```shell
make createdb
```

### Rode a aplicação para servir a API

Rode um container da API:

```
make runapi
```

A API ficará acessível em http://localhost:5000, para verificar se está funcionando corretamente execute em um outro
console no host:

```shell
curl -i http://localhost:8000/api/products
```

A resposta de sucesso deve ser parecida com a seguinte:

```
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 26
Server: Werkzeug/0.16.0 Python/3.7.4
Date: Sat, 12 Oct 2019 14:57:56 GMT

{
  "status": "healthy"
}
```


### Execução de testes

Os seguintes comandos executam os testes da aplicação:

```shell
# roda os testes apresentando a cobertura no terminal
make test

# roda os testes apresentando cobertura como html, requer o firefox instalado
make testcoverage
```


### Interagindo com a API

Crie um token de acesso:

```shell
# abra o console da api
make apiconsole


# cria um token de admin
from flask_jwt_extended import create_access_token
create_access_token(identity={'user_id': 1, 'type': 'admin'})

# cria um token de cliente
create_access_token(identity={'user_id': 1, 'type': 'customer'})
```

Com o token de admin você pode livremente recursos na API já o token de cliente possui algumas limitações de acesso,
por exemplo, não pode criar um produto e não pode gerenciar a Wishlist de outro cliente.

Lembrando que os tokens têm validade de até 15 minutos, caso o token expire basta gerar um novo.

#### Criando um novo Cliente

Exporte seu token e host da API como uma variáveis de ambiente, isso facilitará a escrita das requisições.

```shell
export ACCESS_TOKEN="<TOKEN>"
export API_HOST="http://localhost:5000" # troque se for necessario

curl -XPOST -H "Content-Type: application/json" \
            -H "Authorization: Bearer $ACCESS_TOKEN" \
            "$API_HOST/api/v1/customers" \
            -d '{"name":"Test","email":"email@email.com"}'
```

#### Criando um novo Produto

```shell
curl -XPOST -H "Content-Type: application/json" \
            -H "Authorization: Bearer $ACCESS_TOKEN" \
            "$API_HOST/api/v1/products" \
            -d '{"title":"IPhone","brand":"Apple","image":"https://via.placeholder.com/150","price_cents":10000,"review_score":5}'
```

#### Adicionando um novo Produto na Wishlist de um Cliente

```shell
curl -XPOST -H "Content-Type: application/json" \
            -H "Authorization: Bearer $ACCESS_TOKEN" \
            "$API_HOST/api/v1/customers/<ID DO CLIENTE>/wishlist/items" \
            -d '{"product_id":<ID DO PRODUTO>}'
```

#### Obtendo a Wishlist de um Cliente

```shell
curl -XGET -H "Authorization: Bearer $ACCESS_TOKEN" \
           "$API_HOST/api/v1/customers/<ID DO CLIENTE>/wishlist"
```

#### Removendo um Item da Wishlist

```shell
curl -XDELETE -H "Authorization: Bearer $ACCESS_TOKEN" \
              "$API_HOST/api/v1/customers/<ID DO CLIENTE>/wishlist/items/<ID DO ITEM>"
```
